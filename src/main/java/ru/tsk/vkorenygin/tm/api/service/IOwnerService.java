package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.api.repository.IOwnerRepository;
import ru.tsk.vkorenygin.tm.entity.AbstractOwnerEntity;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IOwnerRepository<E> {

}
