package ru.tsk.vkorenygin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.entity.AbstractOwnerEntity;
import ru.tsk.vkorenygin.tm.exception.AbstractException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IOwnerRepository<E extends AbstractOwnerEntity> {

    @NotNull
    E add(@NotNull final E entity) throws AbstractException;

    boolean existsById(@NotNull final String id, @NotNull final String userId);

    boolean existsByIndex(final int index, @NotNull final String userId);

    @NotNull
    List<E> findAll(@NotNull final String userId);

    @NotNull
    List<E> findAll(@NotNull final Comparator<E> comparator, @NotNull final String userId);

    @NotNull
    Optional<E> findById(@NotNull final String id, @NotNull final String userId) throws AbstractException;

    @NotNull
    Optional<E> findByIndex(final int index, @NotNull final String userId) throws AbstractException;

    @NotNull
    Optional<E> removeById(@NotNull final String id, @NotNull final String userId) throws AbstractException;

    @NotNull
    Optional<E> removeByIndex(final int index, @NotNull final String userId) throws AbstractException;

    @NotNull
    Optional<E> remove(@NotNull final E entity, @NotNull final String userId) throws AbstractException;

    void clear(@NotNull final String userId);

}

