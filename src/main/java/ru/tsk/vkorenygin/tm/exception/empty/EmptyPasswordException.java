package ru.tsk.vkorenygin.tm.exception.empty;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty.");
    }

}
