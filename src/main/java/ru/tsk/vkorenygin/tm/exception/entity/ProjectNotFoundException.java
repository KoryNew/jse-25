package ru.tsk.vkorenygin.tm.exception.entity;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found.");
    }

}
