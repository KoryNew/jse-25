package ru.tsk.vkorenygin.tm.exception.enumerated;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class IncorrectStatusException extends AbstractException {

    public IncorrectStatusException(final Throwable cause) {
        super(cause);
    }

    public IncorrectStatusException(final String value) {
        super("Error! This value '" + value + "' is not a correct status.");
    }

    public IncorrectStatusException() {
        super("Error! status is incorrect.");
    }

}
