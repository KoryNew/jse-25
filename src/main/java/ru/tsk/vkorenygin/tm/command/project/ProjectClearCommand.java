package ru.tsk.vkorenygin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractProjectCommand;
import ru.tsk.vkorenygin.tm.enumerated.Role;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-clear";
    }

    @Override
    public @Nullable String description() {
        return "remove all projects";
    }

    @Override
    public @NotNull Role @NotNull [] roles() {
        return new Role[] {Role.USER};
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull final String currentUserId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().clear(currentUserId);
    }

}
