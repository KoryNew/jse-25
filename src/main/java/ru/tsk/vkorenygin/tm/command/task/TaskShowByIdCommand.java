package ru.tsk.vkorenygin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-show-by-id";
    }

    @Override
    public @Nullable String description() {
        return "show task by id";
    }

    @Override
    public @NotNull Role @NotNull [] roles() {
        return new Role[] {Role.USER};
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Optional<Task> task = serviceLocator.getTaskService().findById(id, currentUserId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        show(task.get());
    }

}
