package ru.tsk.vkorenygin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return "-a";
    }

    @Override
    public @NotNull String name() {
        return "about";
    }

    @Override
    public @Nullable String description() {
        return "display developer info";
    }

    @Override
    public void execute() {
        System.out.println("- ABOUT -");
        @NotNull final String developer = serviceLocator.getPropertyService().getDeveloper();
        System.out.println("Developer: " + developer);
        @NotNull final String email = serviceLocator.getPropertyService().getDeveloperEmail();
        System.out.println("E-mail: " + email);
    }

}
