package ru.tsk.vkorenygin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return "-v";
    }

    @Override
    public @NotNull String name() {
        return "version";
    }

    @Override
    public @Nullable String description() {
        return "display program version";
    }

    @Override
    public void execute() {
        System.out.println("- VERSION -");
        @NotNull final String version = serviceLocator.getPropertyService().getVersion();
        System.out.println(version);
    }

}
