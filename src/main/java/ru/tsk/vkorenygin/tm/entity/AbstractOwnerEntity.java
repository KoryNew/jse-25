package ru.tsk.vkorenygin.tm.entity;

public abstract class AbstractOwnerEntity extends AbstractEntity {

    private String userId = null;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return super.toString() + "User Id: " + getUserId() + "; ";
    }

}
