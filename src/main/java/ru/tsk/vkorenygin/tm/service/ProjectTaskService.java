package ru.tsk.vkorenygin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.api.service.IProjectTaskService;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;
import ru.tsk.vkorenygin.tm.exception.user.AccessDeniedException;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public @NotNull Task bindTaskToProject(final @Nullable String projectId,
                                           final @Nullable String taskId,
                                           final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        if (DataUtil.isEmpty(taskId)) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId, userId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId, userId)) throw new TaskNotFoundException();
        return taskRepository.bindTaskToProjectById(projectId, taskId, userId);
    }

    @Override
    public @NotNull Task unbindTaskFromProject(@Nullable String projectId,
                                               @Nullable String taskId,
                                               @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        if (DataUtil.isEmpty(taskId)) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId, userId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId, userId)) throw new TaskNotFoundException();
        return taskRepository.unbindTaskById(taskId, userId);
    }

    @Override
    public @NotNull List<Task> findAllTasksByProjectId(final @Nullable String id,
                                                       final @Nullable String userId) throws AbstractException  {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (!projectRepository.existsById(id, userId)) throw new ProjectNotFoundException();
        return taskRepository.findAllByProjectId(id, userId);
    }

}
